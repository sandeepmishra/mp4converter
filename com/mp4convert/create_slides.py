'''
Created on Sep 21, 2014

@author:
'''
from optparse import OptionParser;

from com.mp4convert.Constants import Constant;
from com.mp4convert.MP4Converter import MP4Converter;

class CreateSlides :
    def main(self):
        parser = OptionParser(usage="usage: %prog [options] width height input_File", version="%prog 1.0")
        parser.add_option("-z", "--zeroes", default=4, help="number zeroes to be padded, default is 4");
        parser.add_option("-f", "--file_prefix", default="img", help="prefix for images to be generated e.g. img");
        parser.add_option("-e", "--file_ext", default=".png", help="extension of generated images default is png");
        parser.add_option("-i", "--img_mode", default="RGBA", help="image mode - default is RGBA");
        parser.add_option("-d", "--dest", default="", help="video source Destination");


        (options, args) = parser.parse_args()
        if len(args) != 3:
            parser.error("wrong number of arguments");

        print options;
        print args;
        # print "Zeroes are " , sys.argv['ZEROES'];
        print "Zeroes ", options.zeroes;
        Constant.OPTION_ZERO = options.zeroes;
        Constant.OPTION_IMG_EXT = options.file_ext;
        Constant.OPTION_IMG_NAME_PREFIX = options.file_prefix;
        Constant.OPTION_IMAGE_MODE = options.img_mode;
        Constant.OPTION_DESTINATION_PATH = options.dest;
        MP4Converter.execute(int(args[0]), int(args[1]), args[2]);





if __name__ == '__main__':
        CreateSlides().main();
