import Image;
from com.mp4convert.Constants import Constant

class ImageConverter :
    @staticmethod
    def convertImage(width, height, sourcePath, destDirPath):
        ImageConverter.saveImage(sourcePath, destDirPath, width, height);

    @staticmethod
    def saveImage(sourcePath, destFilePath, width, height):
        try:
            im = Image.open(sourcePath);
            size = width, height;
            im = im.resize(size, Image.ANTIALIAS);
            im.save(destFilePath, "PNG");
            ImageConverter.convertImageToRBGA(destFilePath);
        except IOError:
            print "cannot create image";
    @staticmethod
    def convertImageToRBGA(destFilePath):
        im = Image.open(destFilePath);
        im = im.convert("RGBA");
        im.save(destFilePath);
        print im.mode;
    @staticmethod
    def createBlankImage(width, height, sourceDir):
        print "width : height ", width, height;
        im = Image.new(Constant.OPTION_IMAGE_MODE, (width, height));
        im.save(sourceDir+Constant.BLANK_IMAGE, "PNG");


#width = 640;
#height = 480;
#sourcePath = "D:/sandeep/project/mp4_transcoding/ffmpeg-20140815-git-d3a2249-win64-static/bin/jpg-11/images3.jpg";
#destDirPath = "D:/sandeep/project/mp4_transcoding/ffmpeg-20140815-git-d3a2249-win64-static/bin/png-11/imgTest3.png";
#convertImage(width, height, sourcePath, destDirPath);


# Code to reading all images from directory and saving to same name in another directory
#im = Image.open("D:/sandeep/project/mp4_transcoding/ffmpeg-20140815-git-d3a2249-win64-static/bin/jpg-11/images.jpg");
#im = im.resize((640, 480), Image.ANTIALIAS);
#im.save("D:/sandeep/project/mp4_transcoding/ffmpeg-20140815-git-d3a2249-win64-static/bin/png-11/img0003.png");

#for dirpath, dirs, files in os.walk("D:/sandeep/project/mp4_transcoding/ffmpeg-20140815-git-d3a2249-win64-static/bin/png1"):
    #for fileName in files:
        #filePath = os.path.join(dirpath, fileName);
        #print "filePath", filePath
        #im = Image.open(filePath)
        #im = im.resize((640, 480), Image.ANTIALIAS)
        #im.save("D:/sandeep/project/mp4_transcoding/ffmpeg-20140815-git-d3a2249-win64-static/bin/png-11/"+fileName);

