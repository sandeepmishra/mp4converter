import json;
import time;
import datetime;
import glob
import os


from com.mp4convert.ImageConverter import ImageConverter;
from shutil import copyfile;
from com.mp4convert.Constants import Constant;


class MP4Converter:

    @staticmethod
    def execute(width, height, filePath):
        ImageConverter.createBlankImage(width, height, Constant.OPTION_DESTINATION_PATH);
        jsonObj = MP4Converter.loadJSON(filePath);
        prevEndTimeCounter = 0;
        for imageDict in jsonObj[Constant.JSON_IMG_LIST]:
            image = imageDict.get(Constant.JSON_IMG);
            print image.get(Constant.JSON_IMG_FILE_NAME);
            MP4Converter.generateImageFrames(image, prevEndTimeCounter, width, height);
            prevEndTimeCounter = MP4Converter.getTimeinSecond(image.get(Constant.JSON_IMG_END_TIME));

    #Exec command
    #generateSlideShowVideo();
    #clean resource
        MP4Converter.cleanResources(Constant.OPTION_DESTINATION_PATH);

    @staticmethod
    def generateImageFrames(image, prevEndTimeCounter,  width,  height):
        startTimeinSec = MP4Converter.getTimeinSecond(image.get(Constant.JSON_IMG_START_TIME));
        print "Start Time in Seconds:", startTimeinSec;
        endTimeinSec = MP4Converter.getTimeinSecond(image.get(Constant.JSON_IMG_END_TIME));
        print "End Time in Seconds:", endTimeinSec;
        #validate if endTimeCounter is not equals to  image start time .
        if(prevEndTimeCounter != startTimeinSec):
            MP4Converter.generateTransparentImageFrames(prevEndTimeCounter, startTimeinSec - 1, Constant.OPTION_DESTINATION_PATH);
        #GenerateImage

        for count in range(startTimeinSec, endTimeinSec):
            destinationImageName = MP4Converter.generateImageName(count);
            ImageConverter.saveImage(image.get(Constant.JSON_IMG_FILE_NAME), Constant.OPTION_DESTINATION_PATH +destinationImageName, width, height);

    @staticmethod
    def generateImageName(count):
        #{0:04d}
        name = Constant.OPTION_IMG_NAME_PREFIX+(("{0:0" +Constant.OPTION_ZERO + "d}").format(count)) + Constant.OPTION_IMG_EXT;
        print name;
        return name;

    @staticmethod
    def get_image_name(count):
        image_name_format = "{{0:0{0}d}}".format(Constant.OPTION_ZERO)
        image_name = image_name_format.format(count)
        name = Constant.OPTION_IMG_NAME_PREFIX + image_name + Constant.OPTION_IMG_EXT
        return name

    @staticmethod
    def generateTransparentImageFrames(prevEndTimeCounter, startTime, sourceDir):
        #generate transparent image
        #start loop from endTimeCounter to image start time -1
        #copy blank image file to current directory
        for count in range(prevEndTimeCounter, startTime+1):
            copyfile((sourceDir + Constant.BLANK_IMAGE), (sourceDir + MP4Converter.generateImageName(count)));
            print "Transparent Image Generated : ", count;

    # Translate this method
    @staticmethod
    def getTimeinSecond(timeInHours):
        x = time.strptime(timeInHours,'%H:%M:%S');
        timeInseconds = datetime.timedelta(hours=x.tm_hour,minutes=x.tm_min,seconds=x.tm_sec).total_seconds();
        print "Time in Seconds ", int(timeInseconds);
        return int(timeInseconds);

    @staticmethod
    def loadJSON(filePath):
        fileJson = open(filePath);
        jsonStr = "";
        try:
            jsonStr = fileJson.read();
            print jsonStr;
        except :
            print "File not Found Exception : ", filePath;
        return json.loads(jsonStr);

    @staticmethod
    def cleanResources(sourceDir):
        filelist = glob.glob(sourceDir + "*"+ Constant.OPTION_IMG_EXT);
        for f in filelist:
            os.remove(f)
        print sourceDir + "*"+ Constant.OPTION_IMG_EXT, "deleted."

    # Generate video using images
    @staticmethod
    def generateSlideShowVideo():
        command = 'ffmpeg -i background_video.mp4 -r 1 -i img%04d.png -c:v libx264  -pix_fmt yuv420p -filter_complex  fps=fps=30 -filter_complex "nullsrc=size=640x480 [base]; [0:v] setpts=PTS-STARTPTS, scale=640x480 [upperleft]; [1:v] setpts=PTS-STARTPTS, scale=640x480 [upperright]; [base][upperleft] overlay=shortest=1 [tmp1]; [tmp1][upperright] overlay=shortest=1:x=0:y=0" output.mp4';
        print command;



#getTimeinSecond("23:59:60");
MP4Converter.generateImageName(4);
print MP4Converter.get_image_name(4);
#ImageConverter.createBlankImage(640, 480, "D:/sandeep/project/mp4_transcoding/ffmpeg-20140815-git-d3a2249-win64-static/bin/png-11/");
#cleanResources("D:/sandeep/project/mp4_transcoding/ffmpeg-20140815-git-d3a2249-win64-static/bin/png-11/");
#inputJSONFile = 'D:/sandeep/project/mp4_transcoding/ffmpeg-20140815-git-d3a2249-win64-static/bin/jpg-11/json.txt';
#MP4Converter.execute(inputJSONFile, 640, 480);




