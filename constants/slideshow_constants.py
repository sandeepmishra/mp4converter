'''
Created on Sep 20, 2014

@author:
'''

class SlideShowConstants:

    BLANK_IMAGE = "blank.png"
    JSON_IMG_LIST = "images"
    JSON_IMG = "image"
    JSON_IMG_FILE_NAME = "local_file"
    JSON_IMG_START_TIME = "st"
    JSON_IMG_END_TIME = "et"
    OPTION_IMG_NAME_PREFIX = "img"
    OPTION_IMG_EXT = ".png"
    OPTION_ZERO = "4"
    OPTION_IMAGE_MODE = "RGBA"
    TEMP_IMG = "temp.png"