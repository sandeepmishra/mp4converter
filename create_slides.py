'''
Created on Sep 21, 2014

@author: Sandeep
'''
import sys

from optparse import OptionParser
from constants.slideshow_constants import SlideShowConstants
from utils.slideshow_helper import SlideShowHelper
from log.logger import ImageLogger

class CreateSlides :

    def main(self):
        '''
        Main method to create the image slides for slide show
        '''
        ImageLogger.init_logger()

        parser = OptionParser(usage="usage: %prog [options] width height input_file", version="%prog 1.0")
        parser.add_option("-z", "--zeroes", default=4, help="number zeroes to be padded, default is 4")
        parser.add_option("-f", "--file_prefix", default="img", help="prefix for images to be generated e.g. img")
        parser.add_option("-e", "--file_ext", default=".png", help="extension of generated images default is png")
        parser.add_option("-i", "--img_mode", default="RGBA", help="image mode - default is RGBA")
        parser.add_option("-d", "--dest", default="", help="video source destination")
        parser.add_option("-t", "--length", default=0, help="Length of video in seconds")


        (options, args) = parser.parse_args()
        if len(args) != 3:
            parser.error("wrong number of arguments")
            ImageLogger.log_error("wrong number of arguments")

        # Initialize Constants
        SlideShowConstants.OPTION_ZERO = options.zeroes
        SlideShowConstants.OPTION_IMG_EXT = options.file_ext
        SlideShowConstants.OPTION_IMG_NAME_PREFIX = options.file_prefix
        SlideShowConstants.OPTION_IMAGE_MODE = options.img_mode

        ImageLogger.log_info("Creating Slide Show Images")
        success = SlideShowHelper().create_slide_show(int(args[0]), int(args[1]), args[2], options.dest, options.length)
        if success:
            ImageLogger.log_info("Creating Slide Show Images - Complete")
            sys.exit(0)
        else:
            ImageLogger.log_info("Creating Slide Show Images - Failed")
            sys.exit(-1)
        
if __name__ == '__main__':
    CreateSlides().main()
