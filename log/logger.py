import os, traceback, logging
from logging.handlers import RotatingFileHandler

class ImageLogger(object):
    '''
    Image Logger - Logging all image events
    '''

    @staticmethod
    def init_logger():
        '''
        Initialize the image logger and log handler
        '''

        LOG_FILE = os.path.join('imaging.log')

        logparam = {}
        handlerparam = {}
        LOG_FORMAT = '[%(asctime)s] %(name)-12s %(levelname)-8s : %(message)s'

        LOG_DATE_FORMAT = '%Y-%m-%d %H:%M:%S %z'
        LOG_MAX_BYTES = 50 * 1024 * 1024   # 2MB
        LOG_BACKUP_COUNT = 2

        formatter = logging.Formatter(LOG_FORMAT)

        logparam['format'] = LOG_FORMAT
        logparam['level'] = logging.INFO
        logparam['datefmt'] = LOG_DATE_FORMAT

        handlerparam['maxBytes'] = LOG_MAX_BYTES
        handlerparam['backupCount'] = LOG_BACKUP_COUNT

        logging.basicConfig(**logparam)

        handler = RotatingFileHandler(filename=LOG_FILE, **handlerparam)
        handler.setLevel(logging.INFO)
        handler.setFormatter(formatter)

        logger = logging.getLogger("IMAGE_LOGGER")
        logger.addHandler(handler)


    @staticmethod
    def log_debug(message):
        ImageLogger.log_event(logging.DEBUG, message)

    @staticmethod
    def log_info(message):
        ImageLogger.log_event(logging.INFO, message)

    @staticmethod
    def log_warn(message):
        ImageLogger.log_event(logging.WARN, message)
        try:
            if traceback.format_exc().strip() != 'None':
                ImageLogger.log_event(logging.WARN, traceback.format_exc().replace("\n", "::"))
        except Exception as ex:
            ImageLogger.log_event(logging.WARN, ex)

    @staticmethod
    def log_error(message):
        ImageLogger.log_event(logging.ERROR, message)
        try:
            if traceback.format_exc().strip() != 'None':
                ImageLogger.log_event(logging.ERROR, traceback.format_exc().replace("\n", "::"))
        except Exception as ex:
            ImageLogger.log_event(logging.ERROR, ex)

    @staticmethod
    def log_critical(message):
        ImageLogger.log_event(logging.CRITICAL, message)
        try:
            if traceback.format_exc().strip() != 'None':
                ImageLogger.log_event(logging.CRITICAL, traceback.format_exc().replace("\n", "::"))
        except Exception as ex:
            ImageLogger.log_event(logging.CRITICAL, ex)


    @staticmethod
    def log_event(level, message):

        logger = logging.getLogger("IMAGE_LOGGER")

        if level < logging.DEBUG:
            return

        if level == logging.DEBUG:
            logger.debug(message)
        elif level == logging.INFO:
            logger.info(message)
        elif level == logging.WARNING:
            logger.warning(message)
        elif level == logging.WARN:
            logger.warning(message)
        elif level == logging.ERROR:
            logger.error(message)
        elif level == logging.CRITICAL:
            logger.critical(message)