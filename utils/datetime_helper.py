'''
Created on 26-Sep-2014

@author: raghav
'''
import time, datetime

class DateTimeHelper():
    '''
    Data Time Helper Methods
    '''

    @staticmethod
    def get_time_in_second(timeInHours):
        x = time.strptime(timeInHours,'%H:%M:%S')
        time_in_secs = datetime.timedelta(hours=x.tm_hour,minutes=x.tm_min,seconds=x.tm_sec).total_seconds()
        return int(time_in_secs)