import Image
from constants.slideshow_constants import SlideShowConstants

class ImageConverter :
    
    @staticmethod
    def set_size_and_mode(source_path, destination_file, width, height):
        try:
            im = Image.open(source_path)
            size = width, height
            im = im.resize(size, Image.ANTIALIAS)
            im = im.convert(SlideShowConstants.OPTION_IMAGE_MODE)
            im.save(destination_file, "PNG")
        except IOError:
            print "cannot create image"
    
    @staticmethod
    def create_blank_image(filepath, width, height):
        im = Image.new(SlideShowConstants.OPTION_IMAGE_MODE, (width, height))
        im.save(filepath, "PNG")