'''
Created on Sep 21, 2014

@author: Sandeep
'''

import json, os, uuid
from shutil import copyfile

from constants.slideshow_constants import SlideShowConstants
from log.logger import ImageLogger
from utils.datetime_helper import DateTimeHelper
from utils.image_helper import ImageConverter


class SlideShowHelper:
    
    def create_slide_show(self, width, height, input_file, destination, length):
        '''
        We are assuming here that the images in the input file are in order in which they
        are to be displayed.
        '''
        self.width = width
        self.height = height
        self.destination = destination if destination is not None else ""
        self.length = int(length) if length is not None and length.isdigit() else 0
        
        # create destination folder if missing
        if not os.path.exists(destination):
            try:
                os.makedirs(destination)
            except Exception as ex:
                ImageLogger.log_error(ex)
                return False
        
        self.blank_image = os.path.join(self.destination, SlideShowConstants.BLANK_IMAGE)        
        try:
            # create default blank image of required width and height
            ImageConverter.create_blank_image(self.blank_image, width, height)
        
            # load the json input file
            json_file = open(input_file)
            images = json.load(json_file)
            
        except Exception as ex:
            ImageLogger.log_error(ex)
            return False
        
        # create the slide show
        last_count = 0
        for image in images[SlideShowConstants.JSON_IMG_LIST]:
            image = image.get(SlideShowConstants.JSON_IMG)
            success = self.generate_image_frames(image, last_count, width, height)
            if not success:
                return False
            last_count = DateTimeHelper.get_time_in_second(image.get(SlideShowConstants.JSON_IMG_END_TIME))
            
        # create blank image copies until length of video
        if self.length > last_count:
            self.copy_blank_frames(last_count, self.length)
            
        # remove blank image after slide show images are generated
        try:
            os.remove(self.blank_image)
        except Exception as ex:
            ImageLogger.log_error(ex)
            pass
        
        return True

    def generate_image_frames(self, image, last_end_time,  width,  height):
        '''
        Generate image frames for an image from start time to end time
        In case start time does not immediately start after last end time then
        create blank images for the gap between them
        '''

        start_time = DateTimeHelper.get_time_in_second(image.get(SlideShowConstants.JSON_IMG_START_TIME))
        end_time = DateTimeHelper.get_time_in_second(image.get(SlideShowConstants.JSON_IMG_END_TIME))

        #validate if endTimeCounter is not equals to  image start time .
        if(last_end_time != start_time):
            self.copy_blank_frames(last_end_time, start_time)
        
        #GenerateImage
        ImageLogger.log_info("Creating Images from input start ")
        try:
            # Resize and Set Image Mode
            input_path = image.get(SlideShowConstants.JSON_IMG_FILE_NAME)
            input_dir = os.path.dirname(input_path)
            revised_image = "{0}{1}".format(uuid.uuid4(), SlideShowConstants.OPTION_IMG_EXT)
            revised_image = os.path.join(input_dir, revised_image)
            ImageConverter.set_size_and_mode(input_path, revised_image, width, height)
            
            # Generate Image copies for display duration
            for count in range(start_time, end_time):
                dest_image_path = self.get_image_name(count)
                dest_image_path = os.path.join(self.destination, dest_image_path)
                ImageLogger.log_info("{0}: Copying image {1} to {2}".format(count, os.path.basename(input_path), dest_image_path))
                copyfile(revised_image, dest_image_path)
            
            # cleanup - might not be needed
            os.remove(revised_image)
        except Exception as ex:
            ImageLogger.log_error(ex)
            return False
        ImageLogger.log_info("Creating Images from end")
        
        return True
        
    def get_image_name(self, count):
        '''
        Get formatted image name
        '''
        image_name = "{0}{{0:0{1}d}}{2}".format(SlideShowConstants.OPTION_IMG_NAME_PREFIX, 
                            SlideShowConstants.OPTION_ZERO, SlideShowConstants.OPTION_IMG_EXT)
        return image_name.format(count)

    def copy_blank_frames(self, last_end_time, start_time):
        '''
        start loop from endTimeCounter to image start time
        copy blank image file to current directory
        '''
        for count in range(last_end_time, start_time):
            dest_path = os.path.join(self.destination, self.get_image_name(count))
            ImageLogger.log_info("{0}: Copying image {1} to {2}".format(count, SlideShowConstants.BLANK_IMAGE, dest_path))
            copyfile(self.blank_image, dest_path)

